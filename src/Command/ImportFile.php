<?php

namespace App\Command;

use Exception;
use App\Helpers\FileHelper;
use App\Repository\Elasticsearch\ES;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportFile extends Command
{
    protected static $defaultName = 'elastic:import';

    protected function configure()
    {
        $this->setDescription('Import a file into Elasticsearch from ELASTICSEARCH_IMPORT_FILE_PATH defined in the .env file or specify another path for it.')
            ->setHelp('Please define the index you want to import the data in (e.g. ./bin/console elastic:import index).');

        $this->addArgument('index', InputArgument::REQUIRED, 'The index in Elasticsearch.');
        $this->addArgument('file_path', InputArgument::OPTIONAL, 'The path of the file you want to import.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get the index
        $index = $input->getArgument('index');
        // Get the file path or set the default file path from .env file "ELASTICSEARCH_IMPORT_FILE_PATH"
        $filePath = $input->getArgument('file_path')
            ?? dirname(__DIR__) . '/../' . $_ENV['ELASTICSEARCH_IMPORT_FILE_PATH'];

        $file = new FileHelper($filePath);
        $ESClient = (new ES())->setIndex($index);

        try {
            $file->validate('json');
        } catch (Exception $e) {
            $output->writeln($e->getMessage());
            return 1;
        }

        $fileContents = $file->getContents()->toArray();

        $output->writeln([
            'Importing the file',
            '==================',
            '',
        ]);

        if (!$ESClient->exists()) {
            $ESClient->create();
        }

        // The file structure must be reviewed later :)
        if (!array_key_exists('hits', $fileContents)) {
            $output->writeln('The file content must be a json including "hits->hits" keys');
            return 1;
        }

        if (!array_key_exists('hits', $fileContents['hits'])) {
            $output->writeln('The file content must be a json including "hits->hits" keys');
            return 1;
        }

        $ESClient->bulkAdd($fileContents['hits']['hits']);

        $output->writeln('Success!');
        return 0;
    }
}
