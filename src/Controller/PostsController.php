<?php

namespace App\Controller;

use App\Repository\Elasticsearch\ES;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class PostsController
{
    protected $ESClient;

    public function __construct()
    {
        $this->ESClient = (new ES())->setIndex('posts');
    }

    /**
     * @Route("/rest/get-objects-from-selected-tags/{type}", name="get_objects_by_tags", methods={"GET"})
     * @param string $type
     *
     * @return JsonResponse
     */
    public function getByType(string $type)
    {
        $response = $this->ESClient->sortBy('sortingpriority', 'asc')->whereType($type);

        $mappedResponse = [];
        foreach ($response['hits'] as $item) {
            $mappedResponse[$item['_id']] = $item['_source'];
        }

        return new JsonResponse([
            $mappedResponse
        ], 200);
    }

    /**
     * @Route("/rest/get-facets-of-tags/{tagIds}", defaults={"tagIds"=null}, name="get_tags_facets", methods={"GET"})
     * @param ?string $tagIds
     *
     * @return JsonResponse
     */
    public function getByTagIds(?string $tagIds)
    {
        $mappedResponse = [];
        if ($tagIds) {
            $tags     = explode(',', $tagIds);
            // Not sure about the endpoint response
            // it can be all the posts which have requested $tagIds in their 'tags' field
            $response = $this->ESClient->sortBy('sortingpriority', 'asc')->whereIn('tags', $tags);
            // or it can show number of the posts which have these $tagIds in their 'tags' field
            // $response = $this->ESClient->whereInFacets('tags', $tags);
        } else {
            $response = $this->ESClient->sortBy('sortingpriority', 'asc')->getAll();
        }

        foreach ($response['hits'] as $item) {
            $mappedResponse[$item['_id']] = $item['_source'];
        }

        return new JsonResponse([
            $mappedResponse
        ], 200);
    }
}
