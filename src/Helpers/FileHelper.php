<?php

namespace App\Helpers;

use Exception;

class FileHelper
{
    protected $path;

    protected $content;

    /**
     * FileHelper constructor.
     *
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param string $extension
     *
     * @return $this
     * @throws Exception
     */
    public function validate(string $extension): object
    {
        if (!file_exists($this->path)) {
            throw new Exception('The file does not exist!');
        }

        if (pathinfo($this->path, PATHINFO_EXTENSION) !== $extension) {
            throw new Exception(sprintf('The file is not a "%s" file!', $extension));
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function getContents(): object
    {
        $this->content = file_get_contents($this->path);

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return json_decode($this->content, true);
    }

    /**
     * @return object
     */
    public function toJson(): object
    {
        return json_decode($this->content);
    }
}
