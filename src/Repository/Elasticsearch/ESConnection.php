<?php

namespace App\Repository\Elasticsearch;

use Elasticsearch\ClientBuilder;

class ESConnection
{
    protected static $client;

    /**
     * Build a connection
     */
    public static function build()
    {
        if (!is_a(static::$client, ClientBuilder::class)) {
            static::$client = ClientBuilder::create()
                ->setHosts([$_ENV['ELASTICSEARCH_URL']])
                ->build();
        }

        return static::$client;
    }
}
