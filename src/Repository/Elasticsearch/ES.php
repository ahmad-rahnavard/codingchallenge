<?php

namespace App\Repository\Elasticsearch;

class ES
{
    protected $client;

    protected $params;

    // To give you all possible result of queries less than 10000 items
    protected $size = 10000;

    public function __construct()
    {
        $this->client = ESConnection::build();
    }

    /**
     * @param string $index
     *
     * @return $this
     */
    public function setIndex(string $index)
    {
        $this->params['index'] = $index;

        return $this;
    }

    /**
     * @return bool
     */
    public function exists(): bool
    {
        return $this->client->indices()->exists($this->params);
    }

    /**
     * @param array $settings
     *
     * other parameters are
     * ['client','custom','filter_path','human','master_timeout','timeout','update_all_types','wait_for_active_shards']
     *
     * @return string
     */
    public function create(array $settings = ['number_of_shards' => 2, 'number_of_replicas' => 0])
    {
        $this->params['body']['settings'] = $settings;

        return $this->client->indices()->create($this->params);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function bulkAdd(array $data)
    {
        foreach ($data as $row) {
            $this->params['body'][] = [
                'index' => [
                    '_index' => $this->params['index'],
                    '_type'  => $row['_type'],
                    '_id'    => $row['_id']
                ]
            ];

            $this->params['body'][] = $row['_source'];
        }

        return $this->client->bulk($this->params);
    }

    /**
     * @param string $field
     * @param string $direction
     *
     * @return string
     */
    public function sortBy(string $field, string $direction = 'desc')
    {
        $this->params['body']['sort'] = [[$field => ['order' => $direction]],];

        return $this;
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function whereId(string $id)
    {
        $this->params['id'] = $id;

        $elasticResponse = $this->client->search($this->params);

        return $elasticResponse['hits'];
    }

    /**
     * @param string $type
     *
     * @return mixed
     */
    public function whereType(string $type)
    {
        $this->params['type'] = $type;
        $this->params['size'] = $this->size;

        $elasticResponse = $this->client->search($this->params);

        return $elasticResponse['hits'];
    }

    /**
     * @param string $key
     * @param string $value
     * @param string $searchType term|match|match_phrase|wildcard
     *
     * @return mixed
     */
    public function where(string $key, string $value, string $searchType = 'match')
    {
        $this->params['size'] = $this->size;
        $this->params['body'] = [
            'query' => [
                $searchType => [
                    $key => $value
                ]
            ]
        ];

        $elasticResponse = $this->client->search($this->params);

        return $elasticResponse['hits'];
    }

    /**
     * @param string $field
     * @param array  $values
     *
     * @return mixed
     */
    public function whereIn(string $field, array $values)
    {
        $this->params['size'] = $this->size;
        $this->params['body'] = [
            'query' => [
                'terms' => [
                    $field => $values
                ]
            ]
        ];

        $elasticResponse = $this->client->search($this->params);

        return $elasticResponse['hits'];
    }

    /**
     * @param string $field
     * @param array  $values
     *
     * @return mixed
     */
    public function whereInFacets(string $field, array $values)
    {
        $filters = [];
        foreach ($values as $value) {
            $filters[$value] = [
                'terms' => [$field => [$value]]
            ];
        }

        $this->params['size'] = 0;
        $this->params['body'] = [
            'query' => [
                'terms' => [
                    $field => $values
                ]
            ],
            'size'  => 0,
            'aggs'  => [
                'feeds' => [
                    'filters' => [
                        'filters' => $filters
                    ]
                ]
            ]
        ];

        $elasticResponse = $this->client->search($this->params);

        return $elasticResponse['aggregations']['feeds']['buckets'];
    }

    /**
     * @return mixed
     */
    public function getAll()
    {

        $this->params['size'] = $this->size;
        $this->params['body'] = [];
        $elasticResponse = $this->client->search($this->params);

        return $elasticResponse['hits'];
    }
}
